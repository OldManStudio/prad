#include <windows.h>
#include<sstream>
HWND omy, napis;
int wolty = 230;
int Omy = 1;
/* This is where all the input to the window goes to */
using namespace std;
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch(Message) {
		
		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		case WM_COMMAND: {
			if(wParam==1 && Omy>1) {
				Omy--;
				ostringstream ss;
				ss<<Omy;
				string str=ss.str();
				HDC hdc = GetDC(hwnd);
				SetWindowText(omy, str.c_str());
				ReleaseDC(hwnd, hdc);
				
				double wynik = wolty/Omy;
				ostringstream ss2;
				ss2<<wynik;
				string str2 = ss2.str()+"mA";
				SetWindowText(napis, str2.c_str());
			}
			if(wParam==2) {
				Omy++;
				ostringstream ss;
				ss<<Omy;
				string str=ss.str();
				HDC hdc = GetDC(hwnd);
				SetWindowText(omy, str.c_str());
				ReleaseDC(hwnd, hdc);
				
				double wynik = wolty/Omy;
				ostringstream ss2;
				ss2<<wynik;
				string str2 = ss2.str()+"mA";
				SetWindowText(napis, str2.c_str());
			}
			break;
		}
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","Pr�d",WS_VISIBLE|WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, /* x */
		CW_USEDEFAULT, /* y */
		740, /* width */
		480, /* height */
		NULL,NULL,hInstance,NULL);

	napis = CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","230",WS_VISIBLE|WS_CHILD,
		580, /* x */
		170, /* y */
		140, /* width */
		60, /* height */
		hwnd,NULL,hInstance,NULL);
	EnableWindow(napis, false);

	omy = CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","1",WS_VISIBLE|WS_CHILD,
		300, /* x */
		120, /* y */
		140, /* width */
		40, /* height */
		hwnd,NULL,hInstance,NULL);
	EnableWindow(omy, false);
	
	CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","<",WS_VISIBLE|WS_CHILD,
		350, /* x */
		225, /* y */
		20, /* width */
		20, /* height */
		hwnd,(HMENU)1,hInstance,NULL);
	
	CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON",">",WS_VISIBLE|WS_CHILD,
		370, /* x */
		225, /* y */
		20, /* width */
		20, /* height */
		hwnd,(HMENU)2,hInstance,NULL);

	HDC hdc = GetDC(hwnd);
	
	MoveToEx(hdc, 0, 200, NULL);
	LineTo(hdc, 740, 200);
	TextOut(hdc, 0, 180, "230V", 4);
	
	HBRUSH b = CreateSolidBrush(RGB(255, 0, 128));
	HBRUSH stary = (HBRUSH)SelectObject(hdc, b);
	Rectangle(hdc, 350, 180, 390, 220);
	TextOut(hdc, 365, 195, "R", 1);
	ReleaseDC(hwnd, hdc);

	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and 
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
